<?php

namespace App\Entity;

/**
 * Telemetry
 */
class Telemetry
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $user_id;

    /**
     * @var string
     */
    private $device_id;

    /**
     * @var float
     */
    private $sensor_t1;

    /**
     * @var float
     */
    private $sensor_t2;

    /**
     * @var float
     */
    private $sensor_t3;

    /**
     * @var float
     */
    private $sensor_t4;

    /**
     * @var float
     */
    private $sensor_t5;

    /**
     * @var float
     */
    private $sensor_t6;

    /**
     * @var float
     */
    private $sensor_p;

    /**
     * @var float
     */
    private $sensor_f;

    /**
     * @var float
     */
    private $sensor_r;

    /**
     * @var float
     */
    private $sensor_g;

    /**
     * @var boolean
     */
    private $relay_s1;

    /**
     * @var boolean
     */
    private $relay_s2;

    /**
     * @var boolean
     */
    private $relay_s3;

    /**
     * @var boolean
     */
    private $relay_s4;

    /**
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param string $userId
     *
     * @return Telemetry
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set deviceId
     *
     * @param string $deviceId
     *
     * @return Telemetry
     */
    public function setDeviceId($deviceId)
    {
        $this->device_id = $deviceId;

        return $this;
    }

    /**
     * Get deviceId
     *
     * @return string
     */
    public function getDeviceId()
    {
        return $this->device_id;
    }

    /**
     * Set sensorT1
     *
     * @param float $sensorT1
     *
     * @return Telemetry
     */
    public function setSensorT1($sensorT1)
    {
        $this->sensor_t1 = $sensorT1;

        return $this;
    }

    /**
     * Get sensorT1
     *
     * @return float
     */
    public function getSensorT1()
    {
        return $this->sensor_t1;
    }

    /**
     * Set sensorT2
     *
     * @param float $sensorT2
     *
     * @return Telemetry
     */
    public function setSensorT2($sensorT2)
    {
        $this->sensor_t2 = $sensorT2;

        return $this;
    }

    /**
     * Get sensorT2
     *
     * @return float
     */
    public function getSensorT2()
    {
        return $this->sensor_t2;
    }

    /**
     * Set sensorT3
     *
     * @param float $sensorT3
     *
     * @return Telemetry
     */
    public function setSensorT3($sensorT3)
    {
        $this->sensor_t3 = $sensorT3;

        return $this;
    }

    /**
     * Get sensorT3
     *
     * @return float
     */
    public function getSensorT3()
    {
        return $this->sensor_t3;
    }

    /**
     * Set sensorT4
     *
     * @param float $sensorT4
     *
     * @return Telemetry
     */
    public function setSensorT4($sensorT4)
    {
        $this->sensor_t4 = $sensorT4;

        return $this;
    }

    /**
     * Get sensorT4
     *
     * @return float
     */
    public function getSensorT4()
    {
        return $this->sensor_t4;
    }

    /**
     * Set sensorT5
     *
     * @param float $sensorT5
     *
     * @return Telemetry
     */
    public function setSensorT5($sensorT5)
    {
        $this->sensor_t5 = $sensorT5;

        return $this;
    }

    /**
     * Get sensorT5
     *
     * @return float
     */
    public function getSensorT5()
    {
        return $this->sensor_t5;
    }

    /**
     * Set sensorT6
     *
     * @param float $sensorT6
     *
     * @return Telemetry
     */
    public function setSensorT6($sensorT6)
    {
        $this->sensor_t6 = $sensorT6;

        return $this;
    }

    /**
     * Get sensorT6
     *
     * @return float
     */
    public function getSensorT6()
    {
        return $this->sensor_t6;
    }

    /**
     * Set sensorP
     *
     * @param float $sensorP
     *
     * @return Telemetry
     */
    public function setSensorP($sensorP)
    {
        $this->sensor_p = $sensorP;

        return $this;
    }

    /**
     * Get sensorP
     *
     * @return float
     */
    public function getSensorP()
    {
        return $this->sensor_p;
    }

    /**
     * Set sensorF
     *
     * @param float $sensorF
     *
     * @return Telemetry
     */
    public function setSensorF($sensorF)
    {
        $this->sensor_f = $sensorF;

        return $this;
    }

    /**
     * Get sensorF
     *
     * @return float
     */
    public function getSensorF()
    {
        return $this->sensor_f;
    }

    /**
     * Set sensorR
     *
     * @param float $sensorR
     *
     * @return Telemetry
     */
    public function setSensorR($sensorR)
    {
        $this->sensor_r = $sensorR;

        return $this;
    }

    /**
     * Get sensorR
     *
     * @return float
     */
    public function getSensorR()
    {
        return $this->sensor_r;
    }

    /**
     * Set sensorG
     *
     * @param float $sensorG
     *
     * @return Telemetry
     */
    public function setSensorG($sensorG)
    {
        $this->sensor_g = $sensorG;

        return $this;
    }

    /**
     * Get sensorG
     *
     * @return float
     */
    public function getSensorG()
    {
        return $this->sensor_g;
    }

    /**
     * Set relayS1
     *
     * @param boolean $relayS1
     *
     * @return Telemetry
     */
    public function setRelayS1($relayS1)
    {
        $this->relay_s1 = $relayS1;

        return $this;
    }

    /**
     * Get relayS1
     *
     * @return boolean
     */
    public function getRelayS1()
    {
        return $this->relay_s1;
    }

    /**
     * Set relayS2
     *
     * @param boolean $relayS2
     *
     * @return Telemetry
     */
    public function setRelayS2($relayS2)
    {
        $this->relay_s2 = $relayS2;

        return $this;
    }

    /**
     * Get relayS2
     *
     * @return boolean
     */
    public function getRelayS2()
    {
        return $this->relay_s2;
    }

    /**
     * Set relayS3
     *
     * @param boolean $relayS3
     *
     * @return Telemetry
     */
    public function setRelayS3($relayS3)
    {
        $this->relay_s3 = $relayS3;

        return $this;
    }

    /**
     * Get relayS3
     *
     * @return boolean
     */
    public function getRelayS3()
    {
        return $this->relay_s3;
    }

    /**
     * Set relayS4
     *
     * @param boolean $relayS4
     *
     * @return Telemetry
     */
    public function setRelayS4($relayS4)
    {
        $this->relay_s4 = $relayS4;

        return $this;
    }

    /**
     * Get relayS4
     *
     * @return boolean
     */
    public function getRelayS4()
    {
        return $this->relay_s4;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Telemetry
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}

