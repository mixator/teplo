<?php


$app->get('/', function ($request, $response, $args) {

    return $response->withJson(['status'=>'success']);
});


$app->get('/v1/store', function ($request, $response, $args) {
    $telemetry = new \App\Entity\Telemetry();

    $telemetry->setUserId('1');
    $telemetry->setDeviceId( $request->getParam('mac', 0));

    // http://localhost:8004/v1/store?s_t1=1&s_t2=1&s_t3=1&s_t4=1&s_t5=1&s_t6=1&s_p=1&s_f=1&s_r=1&s_g=1&r_s1=1&r_s2=1&r_s3=1&r_s4=1

    $telemetry->setSensorT1($request->getParam('s_t1', 0));
    $telemetry->setSensorT2($request->getParam('s_t2', 0));
    $telemetry->setSensorT3($request->getParam('s_t3', 0));
    $telemetry->setSensorT4($request->getParam('s_t4', 0));
    $telemetry->setSensorT5($request->getParam('s_t5', 0));
    $telemetry->setSensorT6($request->getParam('s_t6', 0));
    $telemetry->setSensorP($request->getParam('s_p', 0));
    $telemetry->setSensorF($request->getParam('s_f', 0));
    $telemetry->setSensorR($request->getParam('s_r', 0));
    $telemetry->setSensorG($request->getParam('s_g', 0));
    $telemetry->setRelayS1($request->getParam('r_s1', 0));
    $telemetry->setRelayS2($request->getParam('r_s2', 0));
    $telemetry->setRelayS3($request->getParam('r_s3', 0));
    $telemetry->setRelayS4($request->getParam('r_s4', 0));
    $telemetry->setCreatedAt(new \DateTime());

    $this->em->persist($telemetry);
    $this->em->flush();
    $this->em->clear();

    return $response->withJson(['status'=>'success', 'id' => $telemetry->getId() ]);
});
