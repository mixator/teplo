<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        'doctrine' => [
            'meta' => [
                'entity_path' => [
                    __DIR__.'/../App/src/Entity'
                ],
                'yaml_path' => [
                    __DIR__.'/../config/yaml_mappings'
                ],
                'auto_generate_proxies' => true,
                'proxy_dir' =>  __DIR__.'/../tmp/proxies',
                'cache' => null,
            ],
            'connection' => [
                'driver'   => 'pdo_pgsql',
                'host'     => 'localhost',
                'dbname'   => 'teplo',
                'user'     => 'postgres',
                'password' => 'Hflscnrfrtn(2',
            ]
        ],
    ],
];
